const express   = require('express');
const app       = express();
const cors      = require('cors');

// MiddleWare:
app.use(express.json())
app.use(cors());
app.listen(6942, () => {
    console.log('connected at 6942');
})


const { MongoClient } = require('mongodb');
const { ObjectId }    = require('mongodb');

const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);
const dbName = 'ToDoList'

let db;
async function main() {
    await client.connect();
    console.log("Network with database is istablished!");
    db = client.db(dbName)
}

main()


// auth related part START
app.get('/LoginChecker', async (req, res) => {
    try{
        const result = await db.collection('users').find({}).toArray()
        res.status(200).json(result)
    }
    catch(err){
        res.status(500).json({Error: err})
    }
})


app.get('/sinUser/:id', async (req, res) => {
    const _id = new ObjectId(req.params.id); 
    try {
        const result = await db.collection('users').find({ _id: _id }).toArray();
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({ Error: err });
    }
});



app.post('/SignupChecker', async (req, res) => {
    try {
        const result = await db.collection('users').insertOne(req.body);
        const userId = result.insertedId;
        res.status(200).json(userId);
    } catch (err) {
        res.status(500).json({ Error: err });
    }
});
// auth related part END  


// Task related part START

app.get('/TaskList/:id', async (req, res) => {
    const UserID = req.params.id
    try{
        const result = await db.collection('Tasks').find({UserID: UserID}).toArray()
        res.status(200).json(result)
    }
    catch(err){
        res.status(500).json({Error: err})
    }
})

app.post('/AddTask', async (req, res) => {
    try{
        await db.collection('Tasks').insertOne(req.body)
        res.status(200).json({msg: 'success'})
    }
    catch(err){
        res.status(500).json({Error: err})
    }
})


app.patch('/TaskToggle/:id', async (req, res) => {
    const _id = new ObjectId(req.params.id)
    try{

        await db.collection('Tasks').updateOne({_id: _id}, {$set: {completed: !req.body.completed}})
        res.status(200).json({msg: 'success'})

    }
    catch(err){
        console.log(err);
    }
})

app.patch('/ModifyTask/:id', async (req, res) => {
    const _id = new ObjectId(req.params.id)
    try{
        await db.collection('Tasks').updateOne({_id: _id}, {$set: req.body})
        res.status(200).json({msg: 'success'})
    }
    catch(err){
        res.status(500).json({Erorr: err})
    }
})


app.delete('/DelteTask/:id', async (req, res) => {
    const _id = new ObjectId(req.params.id)
    try{
        await db.collection('Tasks').deleteOne({_id: _id})
        res.status(200).json({msg: 'success'})
    }
    catch(err){
        res.status(500).json({Erorr: err})
    }
})

// Task related part END  



