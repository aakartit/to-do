import './Todo.css'
import Header from '../toDO/header'
import List from './List'

export default function ToDoList() {
  return (
    <>
        <Header />
        <List />
    </>
  )
}
