import { useRef } from "react";

import { HiOutlineChevronDoubleDown } from "react-icons/hi";


export default function TaskCompleted(props) {


  const taskActivated = (e) => {
    props.onTaskRemove(e)
  }

  const taskCompleted = useRef()
  const arrowDown = useRef()
  const completedListDrop =  () => {
    setTimeout(() => {
      if (taskCompleted.current.classList.contains('TaskCompleted-toggle')) {
          taskCompleted.current.classList.remove('TaskCompleted-toggle')
          arrowDown.current.classList.remove('arrow-down-comp-toggle')
      }
      else{
          taskCompleted.current.classList.add('TaskCompleted-toggle')
          arrowDown.current.classList.add('arrow-down-comp-toggle')
      }
    }, 100);
  }


  return (
    <>
      {props.list.filter(f => f.completed === true).length > 0 && <button  className="completed_btn" onClick={completedListDrop}><span ref={arrowDown}><HiOutlineChevronDoubleDown className="arrow-down-comp"/></span>Completed</button>}
      <div >
        <div ref={taskCompleted} className="TaskCompleted">
          {props.list.map((e, i) => {
            return(
              e.completed === true && (
                  <div className="TaskFather">
                    <div  key={e._id} className='Task'  onClick={() => props.slideOpener(e)} >
                     <span>{e.taskValue}</span> 
                    </div>
                    <div>
                      <input type="checkbox" className="inputCheck" defaultChecked />
                      <span onClick={() => taskActivated(e)} className="check" ></span>
                    </div>
                  </div>
                  )
                  )
          })}
        </div>
      </div>
    </>
  )
}
