import LGC from '../../onlyImages/LGC.svg'
import { logOut } from '../../redux/authSlice'

import { useEffect, useState, useRef } from 'react'
import axios from 'axios'
import { Link, useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

export default function Header(props) {


  const globalState = useSelector(state => state.auth.isLogged)
  const globalId    = useSelector(state => state.auth.userID)

  const navigate    = useNavigate();
  const dispatch    = useDispatch()
  const logoClicked = () => {
    if (!globalState) {
      navigate('/')
    }
  }

  const [UserName, setUserName] = useState('')

  useEffect(() => {
    const fetchData = async () => {

      try{

        const response = await axios.get(`http://localhost:6942/sinUser/${globalId}`)
        setUserName(response.data[0].UserName)

      }
      catch(err){

      }
    }
    fetchData()

  }, [globalState, globalId])





  // UserProfile Logic
  const hiddenDiv = useRef()
  const UserProfileDrop =() => {
    setTimeout(() => {
      hiddenDiv.current.classList.toggle('hiddenDiv-toggle')
      setTimeout(() => {
        if (hiddenDiv.current.classList.contains('hiddenDiv-toggle')) {
          
            hiddenDiv.current.style.top = '5rem'
            hiddenDiv.current.style.opacity = '1'
        }
        else{
          hiddenDiv.current.style.top = '4rem'
          hiddenDiv.current.style.opacity = '0'

        }
      }, 10);
    }, 10);
  }

  const LogOut = () => {
    dispatch(logOut())
    navigate('/')
  }



  return (
    <nav className='NavBar'>
        <img src={LGC} alt=''  onClick={logoClicked}/>
        {globalState 
          
          ? <button className='userProfile' onClick={UserProfileDrop}>{UserName}</button> 

          :  props.login || <Link to={'/Login'}>Login</Link>
          
        }
        <div ref={hiddenDiv} className='hiddenDiv'>
              <button onClick={LogOut}>Log Out</button>
        </div>
    </nav>
  )
}
