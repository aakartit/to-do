import { createSlice } from '@reduxjs/toolkit'


const logged = localStorage.getItem('state')
const id     = localStorage.getItem('userID')

const authSlice = createSlice({

    name: 'auth',
    initialState: {
        isLogged: logged ? logged : false, 
        userID: id ? id : ''
    },
    reducers: {
        logIn:  (state, actions) => {
            localStorage.setItem('state', true)
            localStorage.setItem('userID', actions.payload)
            state.isLogged  = true;
            state.userID    = id;
        },
        logOut: (state) => {
            localStorage.clear()
            state.isLogged = false
            state.userID = ''
        }
    }

})

export const {logIn, logOut} = authSlice.actions;
export default authSlice.reducer;